#!/usr/bin/env python
# coding: utf-8

# In[ ]:


get_ipython().system('pip install tweepy')


# In[ ]:


#Importación de librerías
import tweepy
import time


# In[ ]:


#Definición y cuerpo de las funciones
def conectarATwitter():
    consumer_key = "YOUR_CONSUMER_KEY"
    consumer_secret = "YOUR_CONSUMER_SECRET"
    access_token = "YOUR_ACCESS_TOKEN"
    access_token_secret = "YOUR_ACCESS_TOKEN_SECRET"

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    wrapper = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, retry_count=3, retry_delay=60)
    return wrapper


# In[ ]:


wrapper = conectarATwitter()


# In[ ]:


maximo_id = -1
for i in range(1,16):
    if maximo_id != -1:
        tweets = wrapper.user_timeline(screen_name="@YOUR_ACCOUNT",count=200, include_retweets=False, max_id=maximo_id)
    else:
        tweets = wrapper.user_timeline(screen_name="@YOUR_ACCOUNT",count=200, include_retweets=False)
    for tweet in tweets:
        fecha_formateada = tweet.created_at.strftime("%Y-%m-%dT%H:%M:%S")
        maximo_id = tweet.id
        print(fecha_formateada, " - ", tweet.text)
        try:
            wrapper.destroy_status(tweet.id)
            print("Borrado:", tweet.id)
        except:
            print("Falló al borrar el Tweet con id:", tweet.id)
    time.sleep(60)

